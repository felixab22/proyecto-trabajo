import { CorporativosServices } from './corporativos.service';
import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { DatatableComponent, ColumnMode } from "@swimlane/ngx-datatable";
import { usersListData } from './user-list.data';
@Component({
  selector: 'app-corporativos',
  templateUrl: './corporativos.component.html',
  styleUrls: ['./corporativos.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CorporativosComponent implements OnInit {
  onecrop: any;
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public rows = usersListData;
  public ColumnMode = ColumnMode;
  public limitRef = 10;
  // column header
  public columns = [
    { name: "ID", prop: "ID" },
    { name: "Username", prop: "Username" },
    { name: "Name", prop: "Name" },
    { name: "Last Activity", prop: "Last Activity" },
    { name: "Verified", prop: "Verified" },
    { name: "Role", prop: "Role" },
    { name: "Status", prop: "Status" },
    { name: "Actions", prop: "Actions" },
  ];
  private tempData = [];

  constructor(
    private _corporativosSrv: CorporativosServices
    ) { 

      this.tempData = usersListData;
    }

  ngOnInit(): void {
this.listarCoporativos();
  }
listarCoporativos(){
  this._corporativosSrv.getAPIServiceCorporativos().subscribe((res:any)=>{
    console.log(res);
    this.onecrop = res.data;
  })
}
filterUpdate(event) {
  const val = event.target.value.toLowerCase();

  // filter our data
  const temp = this.tempData.filter(function (d) {
    return d.Username.toLowerCase().indexOf(val) !== -1 || !val;
  });

  // update the rows
  this.rows = temp;
  // Whenever the filter changes, always go back to the first page
  this.table.offset = 0;
}

/**
 * updateLimit
 *
 * @param limit
 */
updateLimit(limit) {
  this.limitRef = limit.target.value;
}
}
