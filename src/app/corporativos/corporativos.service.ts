import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';

@Injectable({providedIn: 'root'})
export class CorporativosServices {
  public apiURL=environment.apiURL;
  public auth_token='Bearer '+localStorage.getItem('tokenscloud');

  constructor(
    private httpClient: HttpClient
  ) {}

      /**
       * OBTIENE EL LISTADO DE PAISES
       */
      getAPIServiceCorporativos(): Observable<any>{

        const httpHeaders: HttpHeaders = new HttpHeaders({
          Authorization: this.auth_token
        });
        return this.httpClient.get<HttpResponse<Object>>(this.apiURL+'/corporativos',{ headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('tokenscloud') }) });
      }
      

}
