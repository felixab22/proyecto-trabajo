import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CorporativosComponent } from './corporativos.component';
import { CorporativosRoutingModule } from './corporativos-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [CorporativosComponent],
  imports: [
    CommonModule,
    CorporativosRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
    NgxDatatableModule,
    AgmCoreModule,
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CorporativosModule { }
